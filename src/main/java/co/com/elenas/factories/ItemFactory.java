package co.com.elenas.factories;

import co.com.elenas.models.ItemModel;

import java.util.List;

public class ItemFactory {

    public static ItemModel create(List<String> data) {
        ItemModel item = new ItemModel();
        item.setNumber(data.get(0));
        item.setTitle(data.get(1));
        item.setDescription(data.get(2));
        return item;
    }


}
