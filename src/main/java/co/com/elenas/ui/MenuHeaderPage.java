package co.com.elenas.ui;

import net.serenitybdd.screenplay.targets.Target;

public class MenuHeaderPage {

    public static final Target OPTION_MY_OFFERS = Target.the("Option my offers").locatedBy("//androidx.appcompat.app.ActionBar.Tab[@content-desc='MY OFFERS']/android.widget.TextView");
    public static final Target OPTION_COUPONS = Target.the("Option coupons").locatedBy("//androidx.appcompat.app.ActionBar.Tab[@content-desc='COUPONS']/android.widget.TextView");
    public static final Target OPTION_AUTO_KING = Target.the("Option AutoKing").locatedBy("//androidx.appcompat.app.ActionBar.Tab[@content-desc='AUTOKING']/android.widget.TextView");

}
