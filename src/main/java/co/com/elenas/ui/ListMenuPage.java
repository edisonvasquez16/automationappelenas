package co.com.elenas.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ListMenuPage {

    public static final Target ITEM_MENU = Target.the("Name menu").locatedBy("//android.widget.TextView[@text='{0}']");
    public static final Target TITLE_MENU = Target.the("Title menu").located(By.id("es.burgerking.android:id/textProductTitle"));
    public static final Target TITLE_DESCRIPTION = Target.the("Description menu").located(By.id("es.burgerking.android:id/textProductDescription"));

}
