package co.com.elenas.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class InitPage {

    public static final Target BTN_INTRO_ENTER = Target.the("Button create account").located(By.id("es.burgerking.android:id/buttonIntroEnter"));
    public static final Target BTN_NEXT_PAGE = Target.the("Button next page").located(By.id("es.burgerking.android:id/textViewCarouselNext"));
    public static final Target BTN_START = Target.the("Button START").located(By.id("es.burgerking.android:id/textViewCarouselNext"));
    public static final Target BTN_ALLOW_NOTIFICATIONS = Target.the("Button allow notifications").located(By.id("es.burgerking.android:id/textPositive"));
    public static final Target BTN_CONFIRM_NOTIFICATIONS = Target.the("Button confirm allow notifications").located(By.id("com.android.packageinstaller:id/permission_allow_button"));
    public static final Target BTN_CLOSE_VIEW = Target.the("Button close view").located(By.id("es.burgerking.android:id/imageViewClose"));

}
