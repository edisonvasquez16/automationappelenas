package co.com.elenas.ui;

import net.serenitybdd.screenplay.targets.Target;

public class MenuFooterPage {

    public static final Target OPTION_START = Target.the("Option start").locatedBy("//android.widget.FrameLayout[@content-desc='Start']/android.widget.ImageView");
    public static final Target OPTION_COUPONS = Target.the("Option coupons").locatedBy("//android.widget.FrameLayout[@content-desc='Coupons']/android.widget.ImageView");
    public static final Target OPTION_MENU = Target.the("Option menu").locatedBy("//android.widget.FrameLayout[@content-desc='Menu']/android.widget.ImageView");
    public static final Target OPTION_SERVICES = Target.the("Option services").locatedBy("//android.widget.FrameLayout[@content-desc='Services']/android.widget.ImageView");
    public static final Target OPTION_PROFILE = Target.the("Option profile").locatedBy("//android.widget.FrameLayout[@content-desc='Profile']/android.widget.ImageView");

}
