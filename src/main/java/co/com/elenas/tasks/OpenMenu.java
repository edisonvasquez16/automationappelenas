package co.com.elenas.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.elenas.ui.ListMenuPage.ITEM_MENU;
import static co.com.elenas.ui.MenuFooterPage.OPTION_MENU;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class OpenMenu implements Task {

    private final String menu;

    public OpenMenu(String menu) {
        this.menu = menu;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(OPTION_MENU),
                Click.on(ITEM_MENU.of(menu))
        );
    }

    public static OpenMenu information(String menu) {
        return instrumented(OpenMenu.class, menu);
    }
}
