package co.com.elenas.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.elenas.ui.InitPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AccessToApp implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BTN_INTRO_ENTER),
                Click.on(BTN_NEXT_PAGE),
                Click.on(BTN_START),
                Click.on(BTN_ALLOW_NOTIFICATIONS),
                Click.on(BTN_CONFIRM_NOTIFICATIONS),
                Click.on(BTN_CLOSE_VIEW)
        );
    }

    public static AccessToApp form() {
        return instrumented(AccessToApp.class);
    }
}
