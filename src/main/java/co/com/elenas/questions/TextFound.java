package co.com.elenas.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;

public class TextFound implements Question<String> {

    private final Target target;

    public TextFound(Target target) {
        this.target = target;
    }

    public static Question<String> in(Target target) {
        return new TextFound(target);
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(target).viewedBy(actor).asString();
    }
}
