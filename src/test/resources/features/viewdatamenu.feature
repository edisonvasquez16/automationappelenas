#Created by Edison Vasquez Burbano

Feature: Use de BurgerKing's app
  As a Android user
  I want to use BurgerKing Application
  To create a new user

  Background:
    Given that a user Edison open the app

  @ViewDetalInformation @smoke
  Scenario Outline: View details of menu
    When he open the information of menu
      | <number> | <title> | <description> |
    Then he see the detail information of item

    Examples:
      | number | title                                                    | description                                                                                                                                                                                                                                                                       |
      | 1      | KING SELECTION - TRUFFLED WITH MUSHROOMS INDIVIDUAL      | We introduce you the new King Selection, 150g of 100% Grilled Angus Beef combined with truffled mayonnaise and cheese, and on top of that mushrooms sauce. All together with fresh tomatoes and Batavia letucce over Brioche Bread. The most addictive burger you´ll ever try.    |
      | 2      | KING SELECTION - TRUFFLED WITH MUSHROOMS DOUBLE          | We introduce you the new King Selection, 300g of 100% Grilled Angus Beef combined with truffled mayonnaise and cheese, and on top of that mushrooms sauce. All together with fresh tomatoes and Batavia letucce over Brioche Bread. The most addictive burger you´ll ever try.    |
      | 3      | KING SELECTION - MENU TRUFFLED WITH MUSHROOMS INDIVIDUAL | We introduce you the new King Selection, 150g of 100% Grilled Angus Beef combined with truffled mayonnaise and cheese, and on top of that mushrooms sauce. All together with fresh tomatoes and Batavia letucce over Brioche Bread. The most addictive burger you´ll ever try.    |
      | 4      | KING SELECTION - MENU TRUFFLED WITH MUSHROOMS DOUBLE     | We introduce you to the new King Selection, 300g of 100% Grilled Angus Beef combined with truffled mayonnaise and cheese, and on top of that mushrooms sauce. All together with fresh tomatoes and Batavia letucce over Brioche Bread. The most addictive burger you´ll ever try. |