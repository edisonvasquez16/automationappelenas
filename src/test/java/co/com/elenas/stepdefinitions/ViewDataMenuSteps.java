package co.com.elenas.stepdefinitions;

import co.com.elenas.factories.ItemFactory;
import co.com.elenas.models.ItemModel;
import co.com.elenas.questions.TextFound;
import co.com.elenas.tasks.AccessToApp;
import co.com.elenas.tasks.OpenMenu;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static co.com.elenas.ui.ListMenuPage.TITLE_DESCRIPTION;
import static co.com.elenas.ui.ListMenuPage.TITLE_MENU;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class ViewDataMenuSteps {

    @Managed(driver = "Appium")
    public WebDriver herMobileDevice;
    private ItemModel item;

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^that a user (.*) open the app$")
    public void openApp(String nameUser) {
        theActorCalled(nameUser).can(BrowseTheWeb.with(herMobileDevice));
    }

    @When("^he open the information of menu$")
    public void openInformation(List<String> data) {
        item = ItemFactory.create(data);
        theActorInTheSpotlight()
                .wasAbleTo(
                        AccessToApp.form(),
                        OpenMenu.information(item.getTitle())
                );
    }

    @Then("^he see the detail information of item$")
    public void seeDetailInformation() {
        theActorInTheSpotlight()
                .should(seeThat(TextFound.in(TITLE_MENU), equalTo(item.getTitle())),
                        seeThat(TextFound.in(TITLE_DESCRIPTION), equalTo(item.getDescription()))
                );
    }
}
