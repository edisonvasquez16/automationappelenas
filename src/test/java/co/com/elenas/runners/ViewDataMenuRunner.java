package co.com.elenas.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/",
        glue = "co.com.elenas.stepdefinitions",
        snippets = SnippetType.CAMELCASE
)

public class ViewDataMenuRunner {
    static AppiumDriverLocalService appiumService = null;

    @BeforeClass
    public static void startAppiumServer() {
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumService.clearOutPutStreams();
    }

    @AfterClass
    public static void stopAppiumServer() {
        appiumService.stop();
    }
}
